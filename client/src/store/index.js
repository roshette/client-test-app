import Vue from "vue";
import Vuex from "vuex";
import * as Mutations from "./mytation-types";
import axios from "axios";

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    providers: [],
    clients: [],
    activeProviders: []
  },
  mutations: {
    [Mutations.SET_CLIENTS](state, payload) {
      state.clients = payload;
    },

    [Mutations.SET_PROVIDERS](state, payload) {
      state.providers = payload;
    },

    [Mutations.ADD_PROVIDER](state, payload) {
      state.providers.push(payload);
    },

    [Mutations.REMOVE_PROVIDER_BY_ID](state, payload) {
      state.providers = state.providers.filter(
        provider => provider._id !== payload.id
      );
    },

    [Mutations.UPDATE_PROVIDER_BY_ID](state, payload) {
      state.providers.forEach(provider => {
        if (provider._id === payload.id) {
          provider.name = payload.name;
        }
      });
    },

    [Mutations.SET_ACTIVE_PROVIDER](state, payload) {
      if (payload.status) {
        state.activeProviders.push(payload.id);
      } else {
        state.activeProviders = state.activeProviders.filter(
          providerId => providerId !== payload.id
        );
      }
    },

    [Mutations.SET_ACTIVE_PROVIDERS](state, payload) {
      state.activeProviders = payload;
    },

    [Mutations.CREATE_CLIENT](state, payload) {
      state.clients.push(payload);
    },

    [Mutations.UPDATE_CLIENT](state, payload) {
      let index = state.clients.findIndex(client => client._id === payload._id);
      Vue.set(state.clients, index, payload);
    },

    [Mutations.REMOVE_CLIENT_BY_ID](state, payload) {
      let index = state.clients.findIndex(client => client._id === payload.id);
      Vue.delete(state.clients, index);
    }
  },
  actions: {
    getClientsTable({ dispatch }) {
      dispatch("getProviders");
      dispatch("getClients");
    },

    async getProviders({ commit }, payload) {
      try {
        let { data } = await axios.get(`http://localhost:3000/providers/`);
        commit(Mutations.SET_PROVIDERS, data);
      } catch (e) {
        console.log(e);
      }
    },

    async getClients({ commit }, payload) {
      try {
        let { data } = await axios.get(`http://localhost:3000/users/`);
        commit(Mutations.SET_CLIENTS, data);
      } catch (e) {
        console.log(e);
      }
    },

    async addProvider({ commit }, payload) {
      try {
        let { data } = await axios.post(`http://localhost:3000/providers/`, {
          name: payload.name
        });
        commit(Mutations.ADD_PROVIDER, data);
      } catch (e) {
        console.log(e);
      }
    },

    async deleteProvider({ commit }, payload) {
      try {
        await axios.delete(`http://localhost:3000/providers/`, {
          data: {
            id: payload.id
          }
        });
        commit(Mutations.REMOVE_PROVIDER_BY_ID, payload);
      } catch (e) {
        console.log(e);
      }
    },

    async updateProvider({ commit }, payload) {
      try {
        await axios.put(`http://localhost:3000/providers/`, {
          id: payload.id,
          name: payload.name
        });
        commit(Mutations.UPDATE_PROVIDER_BY_ID, payload);
      } catch (e) {
        console.log(e);
      }
    },

    setActiveProvider({ commit }, payload) {
      commit(Mutations.SET_ACTIVE_PROVIDER, payload);
    },

    setActiveProviders({ commit }, payload) {
      commit(Mutations.SET_ACTIVE_PROVIDERS, payload);
    },

    async createClient({ state, commit }, payload) {
      try {
        let { data } = await axios.post(`http://localhost:3000/users/`, {
          name: payload.name,
          phone: payload.phone,
          email: payload.email,
          providers: state.activeProviders
        });
        commit(Mutations.CREATE_CLIENT, data);
      } catch (e) {
        console.log(e);
      }
    },

    async updateClient({ state, commit }, payload) {
      try {
        let { data } = await axios.put(`http://localhost:3000/users/`, {
          id: payload.id,
          name: payload.name,
          phone: payload.phone,
          email: payload.email,
          providers: state.activeProviders
        });
        commit(Mutations.UPDATE_CLIENT, data);
      } catch (e) {
        console.log(e);
      }
    },

    async deleteClient({ commit }, payload) {
      try {
        await axios.delete(`http://localhost:3000/users/`, {
          data: { id: payload.id }
        });
        commit(Mutations.REMOVE_CLIENT_BY_ID, payload);
      } catch (e) {
        console.log(e);
      }
    }
  }
});

export default store;

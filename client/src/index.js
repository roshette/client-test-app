import "bootstrap-4-grid";
import "./assets/scss/main.scss";
import store from './store';
import App from "./App.vue";
import Axios from 'axios'

window.Vue = require("vue");

import VCheckbox from "@ui/VCheckbox.vue";
import VButton from "@ui/VButton.vue";
import VInput from "@ui/VInput.vue";
import VPopup from "@ui/VPopup.vue";

Vue.component("v-checkbox", VCheckbox);
Vue.component("v-button", VButton);
Vue.component("v-input", VInput);
Vue.component("v-popup", VPopup);

Vue.prototype.$http = Axios;

const app = new Vue({
  el: "#app",
  store,
  render: h => h(App)
});

const express = require("express");
const mongoose = require("mongoose");
const swaggerJsDoc = require("swagger-jsdoc");
const swaggerUi = require("swagger-ui-express");
const cors = require("cors");
const app = express();
const bodyParser = require("body-parser");
const userRouter = require("./routes/userRouter.js");
const providerRouter = require("./routes/providerRouter.js");
const config = require("./config.js");

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use("/providers", providerRouter);
app.use("/users", userRouter);

const swaggerOptions = {
  swaggerDefinition: {
    info: {
      version: "1.0.0",
      title: "Customer API",
      description: "Customer API Information",
      contact: {
        name: "Roshette",
      },
      servers: ["http://localhost:3000"],
    },
  },
  apis: ["./routes/*"],
};

const swaggerDocs = swaggerJsDoc(swaggerOptions);
app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocs));

app.use(function (req, res, next) {
  res.status(404).send("Not Found");
});

let port = config.port || 3000;

app.use('/static', express.static(__dirname + '/public'));

mongoose.connect(
  "mongodb://localhost:27017/usersdb",
  { useNewUrlParser: true, useUnifiedTopology: true },
  function (err) {
    if (err) return console.log(err);
    app.listen(port, function () {
      console.log(`Server is running on ${port} port`);
    });
  }
);

const express = require("express");
const userController = require("../controllers/userController.js");
const userRouter = express.Router();

/**
 * @swagger
 * /users/:
 *    get:
 *      description: Use to return all users
 *      responses:
 *        '200':
 *          description: return array users
 *        '500':
 *          description: error occured
 */
userRouter.get("/", userController.getUsers);

/**
 * @swagger
 * /users/:
 *    post:
 *      description: Use to create new user
 *      parameters:
 *        - name: name
 *          description: name of user
 *          required: true
 *          schema:
 *            type: string
 *            format: string
 *        - name: email
 *          description: email of user
 *          required: true
 *          schema:
 *            type: string
 *            format: string
 *        - name: phone
 *          description: phone of user
 *          required: true
 *          schema:
 *            type: string
 *            format: string
 *        - name: providers
 *          description: array provider's id 
 *          required: true
 *          schema:
 *            type: array
 *            format: array
 *      responses:
 *        '200':
 *          description: return created user
 *        '500':
 *          description: error occured
 */
userRouter.post("/", userController.createUser);

/**
 * @swagger
 * /users/:
 *    put:
 *      description: Use to update user by id
 *      parameters:
 *        - name: id
 *          description: id of user
 *          required: true
 *          schema:
 *            type: string
 *            format: string
 *        - name: name
 *          description: name of user
 *          required: true
 *          schema:
 *            type: string
 *            format: string
 *        - name: email
 *          description: email of user
 *          required: true
 *          schema:
 *            type: string
 *            format: string
 *        - name: phone
 *          description: phone of user
 *          required: true
 *          schema:
 *            type: string
 *            format: string
 *        - name: providers
 *          description: array provider's id 
 *          required: true
 *          schema:
 *            type: array
 *            format: array
 *      responses:
 *        '200':
 *          description: return updated user
 *        '500':
 *          description: error occured
 */
userRouter.put("/", userController.updateUser);

/**
 * @swagger
 * /users/:
 *    delete:
 *      description: Use to delete user by id
 *      parameters:
 *        - name: id
 *          description: id of user
 *          required: true
 *          schema:
 *            type: string
 *            format: string
 *      responses:
 *        '204':
 *          description: status successfully deleted user
 *        '500':
 *          description: error occured
 */
userRouter.delete("/", userController.deleteUser);
 
module.exports = userRouter;
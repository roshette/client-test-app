const mongoose = require("mongoose");
mongoose.set('useFindAndModify', false); 

const Schema = mongoose.Schema;
const userScheme = new Schema({
    name: String,
    email: String,
    phone: String,
    providers: Array,
});

module.exports = mongoose.model("User", userScheme);
const mongoose = require("mongoose");
mongoose.set('useFindAndModify', false); 
 
const Schema = mongoose.Schema;
const providerScheme = new Schema({
    name: String,
});
module.exports = mongoose.model("Provider", providerScheme);